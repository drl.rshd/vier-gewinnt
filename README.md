# Java Example

[![jdk](https://badgen.net/badge/Java%20SE%20Development%20Kit/14/green)](https://adoptopenjdk.net/?variant=openjdk14&jvmVariant=hotspot)
[![Eclipse](https://badgen.net/badge/Eclipse%20IDE/03/green)](https://www.eclipse.org/downloads/)
[![git](https://badgen.net/badge/git/win/green)](https://git-scm.com/download/win)
[![JavaDoc](https://badgen.net/badge/JavaDoc/14/green)](https://www.oracle.com/java/technologies/javase-jdk14-doc-downloads.html)

# Requirement
- Install the Java Development Kit v14 from this [link](https://adoptopenjdk.net/?variant=openjdk14&jvmVariant=hotspot)
- Install the Eclipse IDE v3 from this [link](https://www.eclipse.org/downloads/)
- Install the git for windows from this [link](https://git-scm.com/download/win)
- Install Java Dokumentation (nur Download und entpacken) [link](https://www.oracle.com/java/technologies/javase-jdk14-doc-downloads.html)

# How To Use
Bitte lesen Sie (übung_01)
