package tuc.isse.VierGewinnt;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import tuc.isse.VierGewinnt.controller.ConsoleGame;
import tuc.isse.VierGewinnt.controller.MocPlayer;
import tuc.isse.VierGewinnt.model.GameObjekt.*;
import tuc.isse.VierGewinnt.model.ObservableBoard;
import tuc.isse.VierGewinnt.model.exceptions.*;
import tuc.isse.VierGewinnt.view.BoardObserver;
import tuc.isse.VierGewinnt.view.VierGewinntFrame;

public class MocGameTest implements BoardObserver {
    private static ObservableBoard board;
    private static ConsoleGame game;

    @Before
    public void setUpBefore() throws Exception {
        board = new ObservableBoard();
        game = new ConsoleGame(board);

        board.addObserver(this);
    }

    @Test
    public void testConsoleGame() throws ColumnFullException, IllegalMoveException {
        MocPlayer player1 = new MocPlayer(Color.RED, board, 2);
        MocPlayer player2 = new MocPlayer(Color.YELLOW, board, 3);
        game.doGame(player1, player2);
        assertEquals(Winner.RED, board.testVictory());
    }

    @Test
    public void testGUI() throws ColumnFullException, IllegalMoveException {
        board.dropToken(board.getToken(Color.RED), 0);
        board.dropToken(board.getToken(Color.YELLOW), 0);
        board.dropToken(board.getToken(Color.RED), 0);
        board.dropToken(board.getToken(Color.YELLOW), 0);
        board.dropToken(board.getToken(Color.RED), 0);
        board.dropToken(board.getToken(Color.YELLOW), 0);

        board.dropToken(board.getToken(Color.RED), 1);
        board.dropToken(board.getToken(Color.YELLOW), 1);
        board.dropToken(board.getToken(Color.RED), 1);
        board.dropToken(board.getToken(Color.YELLOW), 1);
        board.dropToken(board.getToken(Color.RED), 1);
        board.dropToken(board.getToken(Color.YELLOW), 1);

        board.dropToken(board.getToken(Color.RED), 2);
        board.dropToken(board.getToken(Color.YELLOW), 2);
        board.dropToken(board.getToken(Color.RED), 2);
        board.dropToken(board.getToken(Color.YELLOW), 2);
        board.dropToken(board.getToken(Color.RED), 2);
        board.dropToken(board.getToken(Color.YELLOW), 2);

        board.dropToken(board.getToken(Color.YELLOW), 3);
        board.dropToken(board.getToken(Color.RED), 3);
        board.dropToken(board.getToken(Color.YELLOW), 3);
        board.dropToken(board.getToken(Color.RED), 3);
        board.dropToken(board.getToken(Color.YELLOW), 3);
        board.dropToken(board.getToken(Color.RED), 3);

        board.dropToken(board.getToken(Color.RED), 4);
        board.dropToken(board.getToken(Color.YELLOW), 4);
        board.dropToken(board.getToken(Color.RED), 4);
        board.dropToken(board.getToken(Color.YELLOW),4);
        board.dropToken(board.getToken(Color.RED), 4);
        board.dropToken(board.getToken(Color.YELLOW), 4);

        board.dropToken(board.getToken(Color.RED), 5);
        board.dropToken(board.getToken(Color.YELLOW), 5);
        board.dropToken(board.getToken(Color.RED), 5);
        board.dropToken(board.getToken(Color.YELLOW),5);
        board.dropToken(board.getToken(Color.RED), 5);
        board.dropToken(board.getToken(Color.YELLOW), 5);

        board.dropToken(board.getToken(Color.RED), 6);
        board.dropToken(board.getToken(Color.YELLOW), 6);
        board.dropToken(board.getToken(Color.RED), 6);
        board.dropToken(board.getToken(Color.YELLOW),6);
        board.dropToken(board.getToken(Color.RED), 6);
        board.dropToken(board.getToken(Color.YELLOW), 6);

        VierGewinntFrame frame = new VierGewinntFrame(board);
        board.addObserver(frame);
    }

    @Override
    public void update(ObservableBoard board) {
        assert(board != null);
    }
}
