/*
 * muhammad.daryl.rashad@tu-clausthal.de
 * Vorname: Muhammad Daryl
 * Nachname: Rashad
 *
 * mehmet.talha.kocaer@tu-clausthal.de
 * Vorname: Kocaer Mehmet
 * Nachname: Talha
 */
package tuc.isse.VierGewinnt;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import tuc.isse.VierGewinnt.model.Board;
import tuc.isse.VierGewinnt.model.GameObjekt;
import tuc.isse.VierGewinnt.model.GameObjekt.Color;
import tuc.isse.VierGewinnt.model.exceptions.ColumnFullException;
import tuc.isse.VierGewinnt.model.exceptions.IllegalMoveException;

/**
 * Test cases for toString Method
 *
 * @author Muhammad Daryl Rashad
 * @version 1.1.0
 */
public class VierGewinntTest {
    private static Board testObject;

    @Before
    public void setUpBefore() throws Exception {
        testObject = new Board();
    }

    @Test
    public void testOne() throws ColumnFullException, IllegalMoveException {
        testObject.dropToken(testObject.getToken(Color.YELLOW), 2);
        testObject.dropToken(testObject.getToken(Color.RED), 2);

        testObject.dropToken(testObject.getToken(Color.YELLOW), 3);

        testObject.dropToken(testObject.getToken(Color.YELLOW),4);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 4);
        testObject.dropToken(testObject.getToken(Color.RED), 4);

        testObject.dropToken(testObject.getToken(Color.YELLOW), 5);
        testObject.dropToken(testObject.getToken(Color.RED), 5);

        testObject.dropToken(testObject.getToken(Color.RED), 6);
        testObject.dropToken(testObject.getToken(Color.RED), 6);
        testObject.dropToken(testObject.getToken(Color.RED), 6);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 6);

        String soll = "[ ] [ ] [ ] [ ] [ ] [ ] [ ] \n" +
                "[ ] [ ] [ ] [ ] [ ] [ ] [ ] \n" +
                "[ ] [ ] [ ] [ ] [ ] [ ] [O] \n" +
                "[ ] [ ] [ ] [ ] [X] [ ] [X] \n" +
                "[ ] [ ] [X] [ ] [O] [X] [X] \n" +
                "[ ] [ ] [O] [O] [O] [O] [X] ";

        assertEquals(soll, testObject.toString());
        assertEquals(GameObjekt.Winner.YELLOW, testObject.testVictory());
    }

    @Test
    public void testTwo() throws ColumnFullException, IllegalMoveException {
        testObject.dropToken(testObject.getToken(Color.YELLOW), 0);

        testObject.dropToken(testObject.getToken(Color.RED), 1);

        testObject.dropToken(testObject.getToken(Color.YELLOW), 2);
        testObject.dropToken(testObject.getToken(Color.RED), 2);

        testObject.dropToken(testObject.getToken(Color.RED), 3);

        testObject.dropToken(testObject.getToken(Color.YELLOW), 4);
        testObject.dropToken(testObject.getToken(Color.RED), 4);
        testObject.dropToken(testObject.getToken(Color.RED), 4);
        testObject.dropToken(testObject.getToken(Color.RED), 4);
        testObject.dropToken(testObject.getToken(Color.RED), 4);

        testObject.dropToken(testObject.getToken(Color.YELLOW), 5);
        testObject.dropToken(testObject.getToken(Color.RED), 5);

        testObject.dropToken(testObject.getToken(Color.YELLOW), 6);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 6);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 6);

        String soll = "[ ] [ ] [ ] [ ] [ ] [ ] [ ] \n" +
                "[ ] [ ] [ ] [ ] [X] [ ] [ ] \n" +
                "[ ] [ ] [ ] [ ] [X] [ ] [ ] \n" +
                "[ ] [ ] [ ] [ ] [X] [ ] [O] \n" +
                "[ ] [ ] [X] [ ] [X] [X] [O] \n" +
                "[O] [X] [O] [X] [O] [O] [O] ";

        assertEquals(soll, testObject.toString());
        assertEquals(GameObjekt.Winner.RED, testObject.testVictory());
    }

    @Test
    public void testThree() throws ColumnFullException, IllegalMoveException {
        testObject.dropToken(testObject.getToken(Color.YELLOW), 3);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 3);
        testObject.dropToken(testObject.getToken(Color.RED), 3);
        testObject.dropToken(testObject.getToken(Color.RED), 3);

        testObject.dropToken(testObject.getToken(Color.YELLOW),4);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 4);
        testObject.dropToken(testObject.getToken(Color.RED), 4);

        testObject.dropToken(testObject.getToken(Color.YELLOW), 5);
        testObject.dropToken(testObject.getToken(Color.RED), 5);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 5);

        testObject.dropToken(testObject.getToken(Color.RED), 6);
        testObject.dropToken(testObject.getToken(Color.RED), 6);
        testObject.dropToken(testObject.getToken(Color.RED), 6);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 6);

        String soll = "[ ] [ ] [ ] [ ] [ ] [ ] [ ] \n" +
                "[ ] [ ] [ ] [ ] [ ] [ ] [ ] \n" +
                "[ ] [ ] [ ] [X] [ ] [ ] [O] \n" +
                "[ ] [ ] [ ] [X] [X] [O] [X] \n" +
                "[ ] [ ] [ ] [O] [O] [X] [X] \n" +
                "[ ] [ ] [ ] [O] [O] [O] [X] ";

        assertEquals(soll, testObject.toString());
        assertEquals(GameObjekt.Winner.RED, testObject.testVictory());
    }

    @Test
    public void testFour() throws ColumnFullException, IllegalMoveException {
        testObject.dropToken(testObject.getToken(Color.RED), 0);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 0);
        testObject.dropToken(testObject.getToken(Color.RED), 0);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 0);
        testObject.dropToken(testObject.getToken(Color.RED), 0);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 0);

        testObject.dropToken(testObject.getToken(Color.RED), 1);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 1);
        testObject.dropToken(testObject.getToken(Color.RED), 1);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 1);
        testObject.dropToken(testObject.getToken(Color.RED), 1);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 1);

        testObject.dropToken(testObject.getToken(Color.RED), 2);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 2);
        testObject.dropToken(testObject.getToken(Color.RED), 2);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 2);
        testObject.dropToken(testObject.getToken(Color.RED), 2);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 2);

        testObject.dropToken(testObject.getToken(Color.YELLOW), 3);
        testObject.dropToken(testObject.getToken(Color.RED), 3);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 3);
        testObject.dropToken(testObject.getToken(Color.RED), 3);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 3);
        testObject.dropToken(testObject.getToken(Color.RED), 3);

        testObject.dropToken(testObject.getToken(Color.RED), 4);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 4);
        testObject.dropToken(testObject.getToken(Color.RED), 4);
        testObject.dropToken(testObject.getToken(Color.YELLOW),4);
        testObject.dropToken(testObject.getToken(Color.RED), 4);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 4);

        testObject.dropToken(testObject.getToken(Color.RED), 5);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 5);
        testObject.dropToken(testObject.getToken(Color.RED), 5);
        testObject.dropToken(testObject.getToken(Color.YELLOW),5);
        testObject.dropToken(testObject.getToken(Color.RED), 5);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 5);

        testObject.dropToken(testObject.getToken(Color.RED), 6);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 6);
        testObject.dropToken(testObject.getToken(Color.RED), 6);
        testObject.dropToken(testObject.getToken(Color.YELLOW),6);
        testObject.dropToken(testObject.getToken(Color.RED), 6);
        testObject.dropToken(testObject.getToken(Color.YELLOW), 6);

        String soll = "[O] [O] [O] [X] [O] [O] [O] \n" +
                "[X] [X] [X] [O] [X] [X] [X] \n" +
                "[O] [O] [O] [X] [O] [O] [O] \n" +
                "[X] [X] [X] [O] [X] [X] [X] \n" +
                "[O] [O] [O] [X] [O] [O] [O] \n" +
                "[X] [X] [X] [O] [X] [X] [X] ";

        assertEquals(soll, testObject.toString());
        assertEquals(GameObjekt.Winner.TIE, testObject.testVictory());
    }
}
