package tuc.isse.VierGewinnt;

import tuc.isse.VierGewinnt.controller.FrameGame;
import tuc.isse.VierGewinnt.controller.FramePlayer;
import tuc.isse.VierGewinnt.model.GameObjekt;
import tuc.isse.VierGewinnt.model.ObservableBoard;
import tuc.isse.VierGewinnt.view.VierGewinntFrame;

public class GameMain_Frame {
    public static void main(String[] args) {
        ObservableBoard board = new ObservableBoard();
        VierGewinntFrame frame = new VierGewinntFrame(board);
        FrameGame controller = new FrameGame(board, frame);
        board.addObserver(frame);
        board.addObserver(controller);

        controller.doGame(new FramePlayer(GameObjekt.Color.RED, board, frame), new FramePlayer(GameObjekt.Color.YELLOW, board, frame));
    }
}
