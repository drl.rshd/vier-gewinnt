/*
 * muhammad.daryl.rashad@tu-clausthal.de
 * Vorname: Muhammad Daryl
 * Nachname: Rashad
 *
 * mehmet.talha.kocaer@tu-clausthal.de
 * Vorname: Kocaer Mehmet
 * Nachname: Talha
 */
package tuc.isse.VierGewinnt.controller;

import tuc.isse.VierGewinnt.model.Board;
import tuc.isse.VierGewinnt.model.exceptions.ColumnFullException;
import tuc.isse.VierGewinnt.model.exceptions.IllegalMoveException;

public abstract class Game {
    protected Board board;
    private Player player1;
    private Player player2;
    private Player currentPlayer;

    public Game(Board board) {
        this.board = board;
    }

    public void setPlayers (Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public Player getCurrentPlayer() {
        return this.currentPlayer;
    }


    protected void swapPlayer() {
        if (this.currentPlayer == this.player1) {
            this.currentPlayer = this.player2;
        } else {
            this.currentPlayer = this.player1;
        }
    }

    public abstract void doGame (Player player1, Player player2) throws ColumnFullException, IllegalMoveException;

    public Player getPlayer1() {
        return this.player1;
    }
}
