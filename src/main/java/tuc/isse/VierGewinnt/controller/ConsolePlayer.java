/*
 * muhammad.daryl.rashad@tu-clausthal.de
 * Vorname: Muhammad Daryl
 * Nachname: Rashad
 *
 * mehmet.talha.kocaer@tu-clausthal.de
 * Vorname: Kocaer Mehmet
 * Nachname: Talha
 */
package tuc.isse.VierGewinnt.controller;

import tuc.isse.VierGewinnt.model.Board;
import tuc.isse.VierGewinnt.model.GameObjekt;
import tuc.isse.VierGewinnt.model.exceptions.ColumnFullException;
import tuc.isse.VierGewinnt.model.exceptions.IllegalMoveException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsolePlayer extends Player{
    public ConsolePlayer(GameObjekt.Color color, Board board) {
        super(color, board);
    }

    public void doTurn() {
        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println(this + ", what is your next move?");

        while (true) {
            try {
                int columnIndex = Integer.parseInt(consoleReader.readLine());
                System.out.println();
                this.doDrop(columnIndex);
                break;
            } catch (IOException e) {
                System.out.println("Please input a correct integer (0-6)!");
            } catch (NumberFormatException e) {
                System.out.println("Input should be integer!\n");
            } catch (ColumnFullException e) {
                System.out.println("The column is already full!\n");
            } catch (IllegalMoveException e) {
                System.out.println("Illegal state!\n");
                break;
            }
        }
    }
}
