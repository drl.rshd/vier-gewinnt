package tuc.isse.VierGewinnt.controller;

import tuc.isse.VierGewinnt.model.Board;
import tuc.isse.VierGewinnt.model.exceptions.ColumnFullException;
import tuc.isse.VierGewinnt.model.exceptions.IllegalMoveException;
import tuc.isse.VierGewinnt.view.VierGewinntFrame;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FramePlayer extends Player implements ActionListener {
    private final VierGewinntFrame view;

    public FramePlayer(Color color, Board board, VierGewinntFrame view) {
        super(color, board);
        this.view = view;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.getView().removeButtonListener(this);
        int columnIndex = Integer.parseInt(e.getActionCommand().substring(4));

        try {
            this.getBoard().dropToken(this.getBoard().getToken(this.getColor()), columnIndex);
        } catch (ColumnFullException ex) {
            JOptionPane.showMessageDialog(this.view, "Column " + columnIndex + " is full!");
            this.getView().addButtonListener(this);
        } catch (IllegalMoveException ex) {
            JOptionPane.showMessageDialog(this.view, "Illegal state!");
        }
    }

    @Override
    public void doTurn() {
        this.getView().addButtonListener(this);
    }

    public VierGewinntFrame getView() {
        return this.view;
    }
}
