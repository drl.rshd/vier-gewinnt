/*
 * muhammad.daryl.rashad@tu-clausthal.de
 * Vorname: Muhammad Daryl
 * Nachname: Rashad
 *
 * mehmet.talha.kocaer@tu-clausthal.de
 * Vorname: Kocaer Mehmet
 * Nachname: Talha
 */
package tuc.isse.VierGewinnt.controller;

import tuc.isse.VierGewinnt.model.Board;
import tuc.isse.VierGewinnt.model.GameObjekt;
import tuc.isse.VierGewinnt.model.exceptions.ColumnFullException;
import tuc.isse.VierGewinnt.model.exceptions.IllegalMoveException;

public class MocPlayer extends Player {
    private Integer move;

    public MocPlayer(GameObjekt.Color color, Board board, Integer move) {
        super(color, board);
        this.move = move;
    }

    public void doTurn() {
        try {
            super.doDrop(move);
        } catch (IllegalMoveException | ColumnFullException e) {
            throw new RuntimeException(e);
        }
    }
}
