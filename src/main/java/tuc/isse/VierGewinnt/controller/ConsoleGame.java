/*
 * muhammad.daryl.rashad@tu-clausthal.de
 * Vorname: Muhammad Daryl
 * Nachname: Rashad
 *
 * mehmet.talha.kocaer@tu-clausthal.de
 * Vorname: Kocaer Mehmet
 * Nachname: Talha
 */
package tuc.isse.VierGewinnt.controller;

import tuc.isse.VierGewinnt.model.Board;

public class ConsoleGame extends Game{
    public ConsoleGame(Board board) {
        super(board);
    }

    public void doGame(Player player1, Player player2){
        this.setPlayers(player1, player2);

        while (this.board.getWinner() == null) {
            this.getCurrentPlayer().doTurn();
            System.out.println(this.board + "\n");
            this.board.testVictory();
            this.swapPlayer();
        }

        System.out.println(this.board.getWinner() + " wins!");
    }
}
