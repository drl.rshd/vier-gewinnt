package tuc.isse.VierGewinnt.controller;

import tuc.isse.VierGewinnt.model.Board;
import tuc.isse.VierGewinnt.model.ObservableBoard;
import tuc.isse.VierGewinnt.view.BoardObserver;
import tuc.isse.VierGewinnt.view.VierGewinntFrame;

import javax.swing.*;

public class FrameGame extends Game implements BoardObserver {
    VierGewinntFrame view;
    public FrameGame(Board board, VierGewinntFrame view) {
        super(board);
        this.view = view;
    }

    @Override
    public void doGame(Player player1, Player player2) {
        this.setPlayers(player1, player2);
        this.view.setCurrentColor(this.getPlayer1().getColor());
        this.getCurrentPlayer().doTurn();
    }

    @Override
    public void update(ObservableBoard board) {
        this.board.testVictory();

        if (this.board.getWinner() == null) {
            this.swapPlayer();
            this.view.setCurrentColor(this.getCurrentPlayer().getColor());
            this.getCurrentPlayer().doTurn();
        } else {
            for (int i = 0; i < this.view.getContentPane().getComponentCount(); i++) {
                this.view.getContentPane().getComponent(i).setEnabled(false);
            }

            JOptionPane.showMessageDialog(this.view, this.board.getWinner() + " won the game!");
        }

    }
}
