/*
 * muhammad.daryl.rashad@tu-clausthal.de
 * Vorname: Muhammad Daryl
 * Nachname: Rashad
 *
 * mehmet.talha.kocaer@tu-clausthal.de
 * Vorname: Kocaer Mehmet
 * Nachname: Talha
 */
package tuc.isse.VierGewinnt.controller;

import tuc.isse.VierGewinnt.model.Board;
import tuc.isse.VierGewinnt.model.GameObjekt;
import tuc.isse.VierGewinnt.model.Token;
import tuc.isse.VierGewinnt.model.exceptions.ColumnFullException;
import tuc.isse.VierGewinnt.model.exceptions.IllegalMoveException;

public abstract class Player extends GameObjekt {
    private final Color color;
    private final Board board;

    public Player(Color color, Board board) {
        this.color = color;
        this.board = board;
    }

    protected void doDrop(int columnIndex) throws IllegalMoveException, ColumnFullException {
        Token token = this.board.getToken(this.color);
        this.board.dropToken(token, columnIndex);
    }

    public Color getColor() {
        return this.color;
    }

    public Board getBoard() {
        return this.board;
    }
    public abstract void doTurn();

    public String toString() {
        return "Player " + color.toString();
    }
}
