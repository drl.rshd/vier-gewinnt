/*
 * muhammad.daryl.rashad@tu-clausthal.de
 * Vorname: Muhammad Daryl
 * Nachname: Rashad
 *
 * mehmet.talha.kocaer@tu-clausthal.de
 * Vorname: Kocaer Mehmet
 * Nachname: Talha
 */
package tuc.isse.VierGewinnt.model;

/**
 * The colored tokens used in the game
 *
 * @author Muhammad Daryl Rashad
 * @version 1.1.0
 */
public class Token extends GameObjekt {
    private Color color;

    public Token(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String toString() {
        switch (this.getColor()) {
            case RED:
                return "X";
            case YELLOW:
                return "O";
            default:
                return " ";
        }
    }
}
