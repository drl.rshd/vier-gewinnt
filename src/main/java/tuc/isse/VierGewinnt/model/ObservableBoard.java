package tuc.isse.VierGewinnt.model;

import tuc.isse.VierGewinnt.model.exceptions.*;
import tuc.isse.VierGewinnt.view.BoardObserver;

import java.util.ArrayList;

public class ObservableBoard extends Board {
    private final ArrayList<BoardObserver> observers;

    public ObservableBoard() {
        super();
        this.observers = new ArrayList<>();
    }
    public void addObserver(BoardObserver observer) {
        this.observers.add(observer);
    }

    public void removeObserver(BoardObserver observer) {
        this.observers.remove(observer);
    }

    public void notifyObserver() {
        for(BoardObserver o : this.observers) {
            o.update(this);
        }
    }

    @Override
    public void dropToken(Token token, int columnIndex) throws ColumnFullException, IllegalMoveException {
        super.dropToken(token, columnIndex);
        this.notifyObserver();
    }
}
