/*
 * muhammad.daryl.rashad@tu-clausthal.de
 * Vorname: Muhammad Daryl
 * Nachname: Rashad
 *
 * mehmet.talha.kocaer@tu-clausthal.de
 * Vorname: Kocaer Mehmet
 * Nachname: Talha
 */
package tuc.isse.VierGewinnt.model;

/**
 * Parent class for the game objects
 *
 * @author Muhammad Daryl Rashad
 * @version 1.1.0
 */
public abstract class GameObjekt {
    public enum Color {
        RED,
        YELLOW;

        public String toString() {
            switch (this.name()) {
                case "RED":
                    return "X";
                case "YELLOW":
                    return "O";
                default:
                    return "NULL";
            }
        }
    }

    public enum Winner {
        RED,
        YELLOW,
        TIE
    }

    public abstract String toString();
}
