/*
 * muhammad.daryl.rashad@tu-clausthal.de
 * Vorname: Muhammad Daryl
 * Nachname: Rashad
 *
 * mehmet.talha.kocaer@tu-clausthal.de
 * Vorname: Kocaer Mehmet
 * Nachname: Talha
 */
package tuc.isse.VierGewinnt.model;

import tuc.isse.VierGewinnt.model.exceptions.ColumnFullException;
import tuc.isse.VierGewinnt.model.exceptions.IllegalMoveException;

import java.util.*;

/**
 * The game-board that contains all the cells
 *
 * @author Muhammad Daryl Rashad
 * @version 1.1.2
 */
public class Board extends GameObjekt {
    private final Cell[][] spielbrett;
    private final Stack<Token> redTokens;
    private final Stack<Token> yellowTokens;
    private final static int rows = 6;
    private final static int columns = 7;
    private Winner winner;

    /**
     * Default constructor class.
     * Initializes a 7*6 board and its empty cell objects.
     */
    public Board() {
        this.spielbrett = new Cell[columns][rows];
        for (int col = 0; col < columns; col++) {
            for (int row = 0; row < rows; row++) {
                this.spielbrett[col][row] = new Cell();
            }
        }

        this.redTokens = new Stack<>();
        this.yellowTokens = new Stack<>();

        for (int i = 0;i < 21;i++) {
            this.redTokens.push(new Token(Color.RED));
            this.yellowTokens.push(new Token(Color.YELLOW));
        }

        this.winner = null;
    }

    public Token getToken(Color color) {
        Token t = null;
        switch(color) {
            case RED:
                t = this.redTokens.pop();
                break;
            case YELLOW:
                t = this.yellowTokens.pop();
                break;
            default:
                break;
        }

        return t;
    }

    private boolean hasToken(Color color) {
        Stack<Token> s;
        switch (color) {
            case RED:
                s = this.redTokens;
                break;
            case YELLOW:
                s = this.yellowTokens;
                break;
            default:
                return false;
        }

        return !s.isEmpty();
    }

    private boolean isTie() {
        return !hasToken(Color.RED) && !hasToken(Color.YELLOW);
    }

    /**
     * Returns true if the column has empty cells, otherwise false.
     * @param columnIndex the index of the column to be checked
     * @return true if the column has empty cells, otherwise false
     */
    public boolean canDrop(int columnIndex) {
        boolean emptyCellExists = false;
        for (Cell c : this.spielbrett[columnIndex]) {
            if (c.getToken() == null) {
                emptyCellExists = true;
                break;
            }
        }

        return emptyCellExists;
    }

    /**
     * Drop a token into a column
     * This method checks if the column has a free cell and proceeds by getting a Token with
     * the selected color and filling the lowest empty cell of the column with the mentioned
     * Token.
     * @param token the Token to be dropped
     */
    public void dropToken(Token token, int columnIndex) throws ColumnFullException, IllegalMoveException {
        if (winner != null) { throw new IllegalMoveException(); }
        if (canDrop(columnIndex)) {
            for(Cell cell : this.spielbrett[columnIndex]) {
                if (cell.getToken() == null) {
                    cell.setToken(token);
                    break;
                }
            }
        } else { throw new ColumnFullException(); }
    }

    public String toString() {
        String s = "";

        for (int row = rows-1; row >= 0; row--) {
            for (int col = 0; col < columns; col++) {
                s += this.spielbrett[col][row].toString() + " ";
            }
            s += "\n";
        }

        s += "[0] [1] [2] [3] [4] [5] [6]\n";

        return s.substring(0, s.length()-1);
    }

    /**
     * Checks if there are 4 consecutive tokens of the same color in a row
     * @param color Color to be tested
     * @return true if 4 consecutive tokens of the same color in a row is found, otherwise false
     */
    private boolean isRowVictory(Color color) {
        for (int row = 0;row < rows;row++) {
            for (int col = 0;col < columns - 3;col++) {
                Token token = this.spielbrett[col][row].getToken();
                if (token != null && token.getColor() == color) {
                    int checkCol = col;
                    int consecutiveTokens = 0;
                    while (token != null && token.getColor() == color && checkCol < columns) {
                        consecutiveTokens++;
                        if (consecutiveTokens == 4) {
                            return true;
                        }
                        checkCol++;
                        token = this.spielbrett[checkCol][row].getToken();
                    }
                }
            }
        }

        return false;
    }

    /**
     * Checks if there are 4 consecutive tokens of the same color in a column
     * @param color Color to be tested
     * @return true if 4 consecutive tokens of the same color in a column is found, otherwise false
     */
    private boolean isColumnVictory(Color color) {
        for (int col = 0; col < columns; col++) {
            for (int row = 0;row < rows - 3;row++) {
                Token token = this.spielbrett[col][row].getToken();
                if (token != null && token.getColor() == color) {
                    int consecutiveTokens = 0;
                    int checkRow = row;
                    while (token != null && token.getColor() == color && checkRow < rows) {
                        consecutiveTokens++;
                        if (consecutiveTokens == 4) {
                            return true;
                        }
                        checkRow++;
                        token = this.spielbrett[col][checkRow].getToken();
                    }
                }
            }
        }

        return false;
    }

    /**
     * Checks if there are 4 consecutive tokens of the same color in a diagonal
     * @param color Color to be tested
     * @return true if 4 consecutive tokens of the same color in a diagonal is found, otherwise false
     */
    private boolean isDiagonalVictory(Color color) {
        for (int row = 0;row < rows;row++) {
            for (int col = 0;col < columns;col++) {
                Token currentToken = this.spielbrett[col][row].getToken();
                if (currentToken != null && currentToken.getColor() == color) {
                    // Top-right
                    if (col < columns - 3  && row < columns - 3) {
                        int consecutiveTokens = 0;
                        int checkRow = row;
                        int checkCol = col;
                        Token checkToken = this.spielbrett[checkCol][checkRow].getToken();
                        while (checkToken != null && checkToken.getColor() == color) {
                            consecutiveTokens++;
                            if (consecutiveTokens == 4) { return true;}
                            checkRow++;
                            checkCol++;
                            checkToken = this.spielbrett[checkCol][checkRow].getToken();
                        }
                    }

                    // Bottom-right
                    if (col < columns - 3 && row >= 3) {
                        int consecutiveTokens = 0;
                        int checkRow = row;
                        int checkCol = col;
                        Token checkToken = this.spielbrett[checkCol][checkRow].getToken();
                        while (checkToken != null && checkToken.getColor() == color) {
                            consecutiveTokens++;
                            if (consecutiveTokens == 4) {
                                return true;
                            }
                            checkRow--;
                            checkCol++;
                            checkToken = this.spielbrett[checkCol][checkRow].getToken();
                        }
                    }

                    // Top-left
                    if (col >= 3 && row < rows - 3) {
                        int consecutiveTokens = 0;
                        int checkRow = row;
                        int checkCol = col;
                        Token checkToken = this.spielbrett[checkCol][checkRow].getToken();
                        while (checkToken != null && checkToken.getColor() == color) {
                            consecutiveTokens++;
                            if (consecutiveTokens == 4) {
                                return true;
                            }
                            checkRow++;
                            checkCol--;
                            checkToken = this.spielbrett[checkCol][checkRow].getToken();
                        }
                    }

                    // Bottom-left
                    if (col >= 3 && row >= 3) {
                        int consecutiveTokens = 0;
                        int checkRow = row;
                        int checkCol = col;
                        Token checkToken = this.spielbrett[checkCol][checkRow].getToken();
                        while (checkToken != null && checkToken.getColor() == color) {
                            consecutiveTokens++;
                            if (consecutiveTokens == 4) {
                                return true;
                            }
                            checkRow--;
                            checkCol--;
                            checkToken = this.spielbrett[checkCol][checkRow].getToken();
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Tests for any win conditions for either red and yellow
     * @return Red if win condition for red found, Yellow if win condition for yellow found, otherwise None
     */
    public Winner testVictory() {
        if (isTie()) {
            this.winner = Winner.TIE;
        }

        if (isRowVictory(Color.RED) || isColumnVictory(Color.RED) || isDiagonalVictory(Color.RED)) {
            this.winner =  Winner.RED;
        }

        if (isRowVictory(Color.YELLOW) || isColumnVictory(Color.YELLOW) || isDiagonalVictory(Color.YELLOW)) {
            this.winner = Winner.YELLOW;
        }

        return this.winner;
    }

    public Winner getWinner() {
        return winner;
    }

    public Color getTokenColor(int row, int column) {
        Token t = this.spielbrett[column][row].getToken();
        if (t != null) {
            return t.getColor();
        } else {
            return null;
        }
    }
}
