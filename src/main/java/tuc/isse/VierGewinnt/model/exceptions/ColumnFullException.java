/*
 * muhammad.daryl.rashad@tu-clausthal.de
 * Vorname: Muhammad Daryl
 * Nachname: Rashad
 *
 * mehmet.talha.kocaer@tu-clausthal.de
 * Vorname: Kocaer Mehmet
 * Nachname: Talha
 */
package tuc.isse.VierGewinnt.model.exceptions;

/**
 * Exception class for a full column
 *
 * @author Muhammad Daryl Rashad
 * @version 1.1.0
 */
public class ColumnFullException extends Exception {

}
