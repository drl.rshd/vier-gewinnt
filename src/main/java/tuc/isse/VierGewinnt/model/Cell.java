/*
 * muhammad.daryl.rashad@tu-clausthal.de
 * Vorname: Muhammad Daryl
 * Nachname: Rashad
 *
 * mehmet.talha.kocaer@tu-clausthal.de
 * Vorname: Kocaer Mehmet
 * Nachname: Talha
 */
package tuc.isse.VierGewinnt.model;

/**
 * Cells that contain the played Tokens
 *
 * @author Muhammad Daryl Rashad
 * @version 1.1.0
 */
public class Cell extends GameObjekt{
    private Token token;

    public Cell() {
        this.token = null;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public String toString() {
        if (token == null) {
            return "[ ]";
        } else {
            return "[" + this.token + "]";
        }
    }
}
