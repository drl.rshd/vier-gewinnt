package tuc.isse.VierGewinnt.view;

import tuc.isse.VierGewinnt.model.ObservableBoard;

public interface BoardObserver {
    public void update(ObservableBoard board);
}
