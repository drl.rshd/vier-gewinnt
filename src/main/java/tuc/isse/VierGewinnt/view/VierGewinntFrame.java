package tuc.isse.VierGewinnt.view;

import tuc.isse.VierGewinnt.model.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class VierGewinntFrame extends JFrame implements BoardObserver {
    private JPanel panel;
    private Board board;
    private JButton[] buttons;
    private JLabel status;
    private GameObjekt.Color currentColor;

    public VierGewinntFrame(Board board) {
        super("Connect Four");
        this.board = board;
        this.panel = new JPanel(new BorderLayout());

        this.status = new JLabel("Current player: " + this.getCurrentColor(), SwingConstants.CENTER);
        this.status.setFont(new Font("Sans Serif", Font.BOLD, 20));
        panel.add(status, BorderLayout.NORTH);

        this.buttons = new JButton[7];
        this.setButtonView();

        this.add(panel);
        this.setSize(700,750);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    private void setButtonView() {
        JPanel buttonsPanel = new JPanel(new GridLayout(1,7));

        for (int i = 0; i < buttons.length; i++) {
            this.buttons[i] = new JButton();
            this.buttons[i].setContentAreaFilled(false);
            this.buttons[i].setBorder(null);
            this.buttons[i].setSize(90, 480);
            JPanel columnPanel = new JPanel(new GridLayout(6, 1));
            for (int j = 5; j >= 0; j--) {
                final int finalI = i;
                final int finalJ = j;
                columnPanel.add(new JPanel() {
                    public void paintComponent(Graphics g) {
                        super.paintComponent(g);
                        if (getBoard().getTokenColor(finalJ, finalI) == GameObjekt.Color.RED) {
                            g.setColor(new Color(175,0,0));
                        } else if (getBoard().getTokenColor(finalJ, finalI) == GameObjekt.Color.YELLOW) {
                            g.setColor(new Color(200, 200, 0));
                        } else {
                            g.setColor(new Color(80, 80,80));
                        }
                        g.fillOval(10,10, 79, 79);
                    }
                });
            }

            this.buttons[i].add(columnPanel);
            this.buttons[i].setActionCommand("drop" + i);
            buttonsPanel.add(this.buttons[i]);
        }

        this.panel.add(buttonsPanel, BorderLayout.CENTER);
    }

    @Override
    public void update(ObservableBoard board) {
    }

    public void addButtonListener(ActionListener listener) {
        for (JButton jb : buttons) {
            jb.addActionListener(listener);
        }
    }

    public void removeButtonListener(ActionListener listener) {
        for (JButton jb : buttons) {
            jb.removeActionListener(listener);
        }
    }

    public Board getBoard() {
        return board;
    }

    public void setCurrentColor(GameObjekt.Color currentColor) {
        this.currentColor = currentColor;
        this.status.setText("Current player: " + this.getCurrentColor());
    }

    public GameObjekt.Color getCurrentColor() {
        return currentColor;
    }
}
