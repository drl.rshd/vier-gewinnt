/*
 * muhammad.daryl.rashad@tu-clausthal.de
 * Vorname: Muhammad Daryl
 * Nachname: Rashad
 *
 * mehmet.talha.kocaer@tu-clausthal.de
 * Vorname: Kocaer Mehmet
 * Nachname: Talha
 */

package tuc.isse.VierGewinnt;

import tuc.isse.VierGewinnt.controller.ConsoleGame;
import tuc.isse.VierGewinnt.controller.ConsolePlayer;
import tuc.isse.VierGewinnt.model.Board;
import tuc.isse.VierGewinnt.model.GameObjekt;

public class GameMain_Console {
    public static void main(String[] args) {
        System.out.println("Starting new game...");
        Board board = new Board();
        System.out.println(board + "\n");

        ConsolePlayer player1 = new ConsolePlayer(GameObjekt.Color.YELLOW, board);
        ConsolePlayer player2 = new ConsolePlayer(GameObjekt.Color.RED, board);

        ConsoleGame game = new ConsoleGame(board);
        game.doGame(player1, player2);
    }
}
