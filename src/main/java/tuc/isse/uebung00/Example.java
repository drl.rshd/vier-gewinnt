package tuc.isse.uebung00;

public class Example {
    /**
     *
     * @param x any integer number
     * @param y any integer number
     * @return the sum of the two input integers
     */
    public static int addition(int x, int y) {
        return x + y;
    }

    /**
     *
     * @param x any integer number
     * @param y any integer number
     * @return the difference of the two input integers
     */
    public static int subtraction(int x, int y) {
        return x - y;
    }
}
