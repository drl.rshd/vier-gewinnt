package tuc.isse.uebung01;

/**
 * Class for first implementation in Java.
 * @author Vorname Nachname
 *
 */
public class HelloWorld {

	/**
	 * Methods justs prints Hello World to the terminal.
	 * @param args No arguments needed.
	 */
	public static void main(String[] args) {
		System.out.println("Hello World!");
	}
}
